
#include <stdio.h>
#include "basics.h"

void	mes_success(char *message) {
	printf("%sSUCCESS%s - %s\n", COLOR_GREEN, COLOR_NO, message);
	//gb_putstr(COLOR_GREEN);
	//gb_putstr("SUCCESS");
	//gb_putstr(COLOR_NO);
	//gb_putstr(" - ");
	//gb_putendl(message);
}

void	mes_failure(char *message) {
	printf("%sFAILURE%s - %s\n", COLOR_RED, COLOR_NO, message);
	//gb_putstr(COLOR_RED);
	//gb_putstr("FAILURE");
	//gb_putstr(COLOR_NO);
	//gb_putstr(" - ");
	//gb_putendl(message);
}

void	mes_warning(char *subtitle, char *message) {
	printf("%s%s%s - %s\n", COLOR_RED, subtitle, COLOR_NO, message);
	//gb_putstr(COLOR_YELLOW);
	//gb_putstr(subtitle);
	//gb_putstr(COLOR_NO);
	//gb_putstr(" - ");
	//gb_putendl(message);
}
