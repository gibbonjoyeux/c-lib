
#include "basics.h"

int					mem_cmp(const void *s1, const void *s2, size_t n) {
	unsigned char	*ptr;
	unsigned char	*ptr_2;
	size_t			cur;

	cur = 0;
	ptr = (unsigned char *)s1;
	ptr_2 = (unsigned char *)s2;
	while (cur < n && ptr[cur] == ptr_2[cur])
		cur++;
	if (cur != n)
		return ((unsigned char)ptr[cur] - (unsigned char)ptr_2[cur]);
	return 0;
}
