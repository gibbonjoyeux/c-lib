
#include "basics.h"

void		*mem_move(void *dst, const void *src, size_t len) {
	size_t	cur;
	uint8_t	*ptr;
	uint8_t	*ptr_2;

	if (dst > src) {
		cur = len;
		ptr = (uint8_t *)dst;
		ptr_2 = (uint8_t *)src;
		while (cur > 0) {
			ptr[cur - 1] = ptr_2[cur - 1];
			cur--;
		}
	} else {
		src = mem_cpy(dst, src, len);
	}
	return (dst);
}
